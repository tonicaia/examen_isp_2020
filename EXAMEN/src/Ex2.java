import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Ex2 {
    public static void main(String[] args) {
        new GUI();
    }
}

class GUI extends JFrame {
    JButton afisBtn;
    JTextField textField1;
    JTextField textField2;

    GUI() {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setLayout(null);
        this.setSize(180, 200);
        this.setTitle("Ex2");

        afisBtn = new JButton("Afiseaza");
        textField1 = new JTextField();
        textField2 = new JTextField();

        textField1.setBounds(10, 10, 100, 30);
        textField2.setBounds(10, 50, 100, 30);
        afisBtn.setBounds(10, 100, 100, 30);

        afisBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String text = textField1.getText();
                int c = 0;
                for (int i = 0; i < text.length(); i++) {
                    if (text.charAt(i) !=' ')
                        c++;
                }
                textField2.setText(c+"");
            }
        });
        this.add(textField1);
        this.add(textField2);
        this.add(afisBtn);
        this.setVisible(true);
    }
}